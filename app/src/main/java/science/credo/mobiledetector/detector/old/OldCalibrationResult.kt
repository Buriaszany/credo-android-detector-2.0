package science.credo.mobiledetector.detector.old

class OldCalibrationResult (
    val blackThreshold : Int,
    val avg : Int,
    val max: Int
)